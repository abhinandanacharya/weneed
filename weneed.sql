-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 21, 2021 at 06:44 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `weneed`
--

-- --------------------------------------------------------

--
-- Table structure for table `ajay@gmail.com`
--

CREATE TABLE `ajay@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `anand@gmail.com`
--

CREATE TABLE `anand@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `annu@gmail.com`
--

CREATE TABLE `annu@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `arjun@gmail.com`
--

CREATE TABLE `arjun@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `arun@gmail.com`
--

CREATE TABLE `arun@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ashok@gmail.com`
--

CREATE TABLE `ashok@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `customer_login`
--

CREATE TABLE `customer_login` (
  `Customer_ID` int(2) NOT NULL,
  `Customer_Name` varchar(30) NOT NULL,
  `Customer_Email` varchar(40) NOT NULL,
  `Customer_Password` varchar(20) NOT NULL,
  `Customer_Phone` varchar(10) NOT NULL,
  `Customer_PIN` int(6) NOT NULL,
  `Customer_Address` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer_login`
--

INSERT INTO `customer_login` (`Customer_ID`, `Customer_Name`, `Customer_Email`, `Customer_Password`, `Customer_Phone`, `Customer_PIN`, `Customer_Address`) VALUES
(1, 'Yash', 'yashramdeo19003@gmail.com', 'test', '7297099799', 342008, 'D-6, Modal Town, Bhatti ki Bawari');

-- --------------------------------------------------------

--
-- Table structure for table `deepak@gmail.com`
--

CREATE TABLE `deepak@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `dev@gmail.com`
--

CREATE TABLE `dev@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `gargi@gmail.com`
--

CREATE TABLE `gargi@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `gopal@gmail.com`
--

CREATE TABLE `gopal@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `imran@gmail.com`
--

CREATE TABLE `imran@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `indumati@gmail.com`
--

CREATE TABLE `indumati@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `jagdish@gmail.com`
--

CREATE TABLE `jagdish@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kiran@gmail.com`
--

CREATE TABLE `kiran@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `lokesh@gmail.com`
--

CREATE TABLE `lokesh@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `lucky@gmail.com`
--

CREATE TABLE `lucky@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `meenakshi@gmail.com`
--

CREATE TABLE `meenakshi@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `nafeesa@gmail.com`
--

CREATE TABLE `nafeesa@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `nandu@gmail.com`
--

CREATE TABLE `nandu@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `nawaaz@gmail.com`
--

CREATE TABLE `nawaaz@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `omprakash@gmail.com`
--

CREATE TABLE `omprakash@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `raju@gmail.com`
--

CREATE TABLE `raju@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `rakesh@gmail.com`
--

CREATE TABLE `rakesh@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `rekha@gmail.com`
--

CREATE TABLE `rekha@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `salman@gmail.com`
--

CREATE TABLE `salman@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `santosh@gmail.com`
--

CREATE TABLE `santosh@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `shanti@gmail.com`
--

CREATE TABLE `shanti@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `shilpa@gmail.com`
--

CREATE TABLE `shilpa@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `surendra@gmail.com`
--

CREATE TABLE `surendra@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tarun@gmail.com`
--

CREATE TABLE `tarun@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `trilok@gmail.com`
--

CREATE TABLE `trilok@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `varun@gmail.com`
--

CREATE TABLE `varun@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `vidya@gmail.com`
--

CREATE TABLE `vidya@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `worker_login`
--

CREATE TABLE `worker_login` (
  `Worker_ID` int(2) NOT NULL,
  `Worker_Name` varchar(30) NOT NULL,
  `Worker_Email` varchar(30) NOT NULL,
  `Worker_Psd` varchar(30) NOT NULL,
  `Worker_Job` varchar(15) NOT NULL,
  `Worker_Phone` varchar(10) NOT NULL,
  `Worker_Gender` varchar(15) NOT NULL,
  `Worker_PIN` int(6) NOT NULL,
  `Worker_AADHAR` varchar(16) NOT NULL,
  `Job_Type` varchar(15) NOT NULL,
  `Experience` int(4) NOT NULL,
  `Service_Cost` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `worker_login`
--

INSERT INTO `worker_login` (`Worker_ID`, `Worker_Name`, `Worker_Email`, `Worker_Psd`, `Worker_Job`, `Worker_Phone`, `Worker_Gender`, `Worker_PIN`, `Worker_AADHAR`, `Job_Type`, `Experience`, `Service_Cost`) VALUES
(1, 'Yash', 'yashramdeo19003@gmail.com', 'yashtest', 'Electrician', '7297099799', 'Male', 342008, '997990791927', 'Part Time', 7, 500),
(2, 'Yanshu', 'yanshupanwar@gmail.com', 'yanshutest', 'Plumber', '7229999772', 'Male', 342008, '784512369874', 'Part Time', 4, 500),
(3, 'Shanti', 'shanti@gmail.com', 'shantitest', 'Maid', '8619066599', 'Female', 342010, '962354187940', 'Full Time', 6, 3500),
(4, 'Vidya', 'vidya@gmail.com', 'vidyatest', 'Maid', '2060099', 'Female', 342008, '321045678916', 'Part Time', 8, 2000),
(5, 'Salman', 'salman@gmail.com', 'salmantest', 'Mechanic', '8387821820', 'Trans', 342008, '203467891033', 'Part Time', 9, 600),
(6, 'Rakesh', 'rakesh@gmail.com', 'rakeshtest', 'Plumber', '9142106202', 'Male', 342008, '734126589014', 'Part Time', 12, 500),
(7, 'Rekha', 'rekha@gmail.com', 'rekhatest', 'Maid', '9414185674', 'Trans', 342008, '654789561234', 'Full Time', 8, 3000),
(8, 'Annu', 'annu@gmail.com', 'annutest', 'Maid', '9156427723', 'Female', 342008, '763456912404', 'Part Time', 9, 1600),
(9, 'Shilpa', 'shilpa@gmail.com', 'shilpatest', 'Maid', '8387824321', 'Female', 342008, '541236527458', 'Full Time', 11, 2800),
(10, 'Deepak', 'deepak@gmail.com', 'deepaktest', 'Maid', '9460141468', 'Male', 342008, '951367482044', 'Part Time', 3, 1500),
(11, 'Nafeesa', 'nafeesa@gmail.com', 'nafeesatest', 'Maid', '806862609', 'Female', 342010, '906268603819', 'Part Time', 5, 1200),
(12, 'Indumati', 'indumati@gmail.com', 'indutest', 'Maid', '6375512318', 'Male', 342010, '918132155736', 'Full Time', 8, 3200),
(13, 'Meenakshi', 'meenakshi@gmail.com', 'meenakshitest', 'Maid', '8003907927', 'Female', 342010, '729709193008', 'Part Time', 4, 1500),
(14, 'Dev Narayan', 'dev@gmail.com', 'devtest', 'Maid', '9413566151', 'Male', 342010, '511665314919', 'Part Time', 7, 2000),
(15, 'Gargi', 'gargi@gmail.com', 'gargitest', 'Maid', '9694302189', 'Female', 342013, '981203491969', 'Part Time', 6, 1800),
(16, 'Jagdish', 'jagdish@gmail.com', 'jagdishtest', 'Maid', '8107144469', 'Male', 342013, '919644417018', 'Part Time', 4, 1200),
(17, 'Kiran', 'kiran@gmail.com', 'kirantest', 'Maid', '9461269913', 'Female', 342013, '319961264919', 'Full Time', 7, 2500),
(18, 'Nandu', 'nandu@gmail.com', 'nandutest', 'Electrician', '9468555663', 'Male', 342008, '366555864920', 'Part Time', 12, 600),
(19, 'Om Prakash', 'omprakash@gmail.com', 'omtest', 'Electrician', '9024507533', 'Male', 342008, '335705420589', 'Part Time', 10, 400),
(21, 'Arun', 'arun@gmail.com', 'aruntest', 'Electrician', '9460530170', 'Male', 342010, '771003506499', 'Part Time', 13, 550),
(22, 'Surendra', 'surendra@gmail.com', 'surendratest', 'Electrician', '9928990109', 'Male', 342010, '919010998299', 'Part Time', 6, 450),
(23, 'Raju', 'raju@gmail.com', 'rajutest', 'Electrician', '9414760999', 'Male', 342013, '139414126979', 'Part Time', 10, 600),
(24, 'Tarun', 'tarun@gmail.com', 'taruntest', 'Electrician', '9610538831', 'Male', 342013, '551388350169', 'Part Time', 15, 400),
(25, 'Lokesh', 'lokesh@gmail.com', 'lokeshtest', 'Electrician', '9414803840', 'Male', 342013, '840038414919', 'Part Time', 12, 650),
(26, 'Anand', 'anand@gmail.com', 'anandtest', 'Mechanic', '7976263108', 'Male', 342008, '801362679917', 'Part Time', 15, 1000),
(27, 'Trilok', 'trilok@gmail.com', 'triloktest', 'Mechanic', '9413461110', 'Male', 342008, '611104313991', 'Part Time', 8, 800),
(28, 'Gopal', 'gopal@gmail.com', 'gopaltest', 'Mechanic', '7014095955', 'Male', 342010, '915595904107', 'Part Time', 13, 1200),
(29, 'Ashok', 'ashok@gmail.com', 'ashoktest', 'Mechanic', '8003343593', 'Male', 342010, '334359300891', 'Part Time', 16, 1000),
(30, 'Nawaaz', 'nawaaz@gmail.com', 'nawaaztest', 'Mechanic', '9462295190', 'Male', 342010, '919015922649', 'Part Time', 8, 600),
(31, 'Imran', 'imran@gmail.com', 'imrantest', 'Mechanic', '7976710161', 'Male', 342010, '976761019170', 'Part Time', 11, 800),
(32, 'Arjun', 'arjun@gmail.com', 'arjuntest', 'Mechanic', '8764035661', 'Male', 342013, '916615104678', 'Part Time', 14, 1200),
(33, 'Lucky', 'lucky@gmail.com', 'luckytest', 'Mechanic', '9413855694', 'Male', 342013, '556948391149', 'Part Time', 12, 750),
(34, 'Santosh', 'santosh@gmail.com', 'santoshtest', 'Plumber', '9828034353', 'Male', 342008, '912809853343', 'Part Time', 12, 600),
(35, 'Varun', 'varun@gmail.com', 'varuntest', 'Plumber', '9414143158', 'Male', 342010, '414315891944', 'Part Time', 15, 500),
(36, 'Zeeshan', 'zeeshan@gmail.com', 'zeeshantest', 'Plumber', '7406346600', 'Male', 342013, '346609107406', 'Part Time', 8, 550),
(37, 'Ajay', 'ajay@gmail.com', 'ajaytest', 'Plumber', '9873920264', 'Male', 342008, '920873926491', 'Part Time', 16, 700);

-- --------------------------------------------------------

--
-- Table structure for table `yanshupanwar@gmail.com`
--

CREATE TABLE `yanshupanwar@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `yashramdeo19003@gmail.com`
--

CREATE TABLE `yashramdeo19003@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `zeeshan@gmail.com`
--

CREATE TABLE `zeeshan@gmail.com` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_address` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ajay@gmail.com`
--
ALTER TABLE `ajay@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `anand@gmail.com`
--
ALTER TABLE `anand@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `annu@gmail.com`
--
ALTER TABLE `annu@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `arjun@gmail.com`
--
ALTER TABLE `arjun@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `arun@gmail.com`
--
ALTER TABLE `arun@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `ashok@gmail.com`
--
ALTER TABLE `ashok@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `customer_login`
--
ALTER TABLE `customer_login`
  ADD PRIMARY KEY (`Customer_ID`),
  ADD UNIQUE KEY `Customer_Email` (`Customer_Email`),
  ADD UNIQUE KEY `Customer_Phone` (`Customer_Phone`);

--
-- Indexes for table `deepak@gmail.com`
--
ALTER TABLE `deepak@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `dev@gmail.com`
--
ALTER TABLE `dev@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `gargi@gmail.com`
--
ALTER TABLE `gargi@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `gopal@gmail.com`
--
ALTER TABLE `gopal@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `imran@gmail.com`
--
ALTER TABLE `imran@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `indumati@gmail.com`
--
ALTER TABLE `indumati@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `jagdish@gmail.com`
--
ALTER TABLE `jagdish@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `kiran@gmail.com`
--
ALTER TABLE `kiran@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `lokesh@gmail.com`
--
ALTER TABLE `lokesh@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `lucky@gmail.com`
--
ALTER TABLE `lucky@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `meenakshi@gmail.com`
--
ALTER TABLE `meenakshi@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `nafeesa@gmail.com`
--
ALTER TABLE `nafeesa@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `nandu@gmail.com`
--
ALTER TABLE `nandu@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `nawaaz@gmail.com`
--
ALTER TABLE `nawaaz@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `omprakash@gmail.com`
--
ALTER TABLE `omprakash@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `raju@gmail.com`
--
ALTER TABLE `raju@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `rakesh@gmail.com`
--
ALTER TABLE `rakesh@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `rekha@gmail.com`
--
ALTER TABLE `rekha@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `salman@gmail.com`
--
ALTER TABLE `salman@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `santosh@gmail.com`
--
ALTER TABLE `santosh@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `shanti@gmail.com`
--
ALTER TABLE `shanti@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `shilpa@gmail.com`
--
ALTER TABLE `shilpa@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `surendra@gmail.com`
--
ALTER TABLE `surendra@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `tarun@gmail.com`
--
ALTER TABLE `tarun@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `trilok@gmail.com`
--
ALTER TABLE `trilok@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `varun@gmail.com`
--
ALTER TABLE `varun@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `vidya@gmail.com`
--
ALTER TABLE `vidya@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `worker_login`
--
ALTER TABLE `worker_login`
  ADD PRIMARY KEY (`Worker_ID`),
  ADD UNIQUE KEY `Worker_Email` (`Worker_Email`),
  ADD UNIQUE KEY `Worker_Phone` (`Worker_Phone`),
  ADD UNIQUE KEY `Worker_AADHAR` (`Worker_AADHAR`);

--
-- Indexes for table `yanshupanwar@gmail.com`
--
ALTER TABLE `yanshupanwar@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `yashramdeo19003@gmail.com`
--
ALTER TABLE `yashramdeo19003@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- Indexes for table `zeeshan@gmail.com`
--
ALTER TABLE `zeeshan@gmail.com`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_number` (`contact_number`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ajay@gmail.com`
--
ALTER TABLE `ajay@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `anand@gmail.com`
--
ALTER TABLE `anand@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `annu@gmail.com`
--
ALTER TABLE `annu@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `arjun@gmail.com`
--
ALTER TABLE `arjun@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `arun@gmail.com`
--
ALTER TABLE `arun@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ashok@gmail.com`
--
ALTER TABLE `ashok@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer_login`
--
ALTER TABLE `customer_login`
  MODIFY `Customer_ID` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `deepak@gmail.com`
--
ALTER TABLE `deepak@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dev@gmail.com`
--
ALTER TABLE `dev@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gargi@gmail.com`
--
ALTER TABLE `gargi@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gopal@gmail.com`
--
ALTER TABLE `gopal@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `imran@gmail.com`
--
ALTER TABLE `imran@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `indumati@gmail.com`
--
ALTER TABLE `indumati@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jagdish@gmail.com`
--
ALTER TABLE `jagdish@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kiran@gmail.com`
--
ALTER TABLE `kiran@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lokesh@gmail.com`
--
ALTER TABLE `lokesh@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lucky@gmail.com`
--
ALTER TABLE `lucky@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `meenakshi@gmail.com`
--
ALTER TABLE `meenakshi@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nafeesa@gmail.com`
--
ALTER TABLE `nafeesa@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nandu@gmail.com`
--
ALTER TABLE `nandu@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nawaaz@gmail.com`
--
ALTER TABLE `nawaaz@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `omprakash@gmail.com`
--
ALTER TABLE `omprakash@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `raju@gmail.com`
--
ALTER TABLE `raju@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rakesh@gmail.com`
--
ALTER TABLE `rakesh@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rekha@gmail.com`
--
ALTER TABLE `rekha@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `salman@gmail.com`
--
ALTER TABLE `salman@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `santosh@gmail.com`
--
ALTER TABLE `santosh@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shanti@gmail.com`
--
ALTER TABLE `shanti@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shilpa@gmail.com`
--
ALTER TABLE `shilpa@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `surendra@gmail.com`
--
ALTER TABLE `surendra@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tarun@gmail.com`
--
ALTER TABLE `tarun@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trilok@gmail.com`
--
ALTER TABLE `trilok@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `varun@gmail.com`
--
ALTER TABLE `varun@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vidya@gmail.com`
--
ALTER TABLE `vidya@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `worker_login`
--
ALTER TABLE `worker_login`
  MODIFY `Worker_ID` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `yanshupanwar@gmail.com`
--
ALTER TABLE `yanshupanwar@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `yashramdeo19003@gmail.com`
--
ALTER TABLE `yashramdeo19003@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `zeeshan@gmail.com`
--
ALTER TABLE `zeeshan@gmail.com`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
