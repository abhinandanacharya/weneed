<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Worker Form</title>
    <link rel="stylesheet" href="proCss.css">
</head>

<body>
    <form action="Work_SignUp_PHP.php" method="post">
    <nav>

<input type="checkbox" id="check">
<label for="check" class="checkbtn">
  <i class="fas fa-bars"></i>
</label>
<a href="project1.php"> <img class="logo" src="logoc.png"></a>
<ul>
    <li><a class="active" href="project1.php">Home</a></li>
    <li><a href="#">About</a></li>
    <li><a href="#">Contact</a></li>

</ul>

<ul style="float: right !important; color: white;">
    <li><a href="Login_Form.php">Login</a>/<a href="CustomerSignUp.html">Sign Up</a></li>

</ul>

</nav>
        <div class="Wform" style="margin-left:33%;">
            <h1>Register</h1>
            <p>Please fill in this form to create an account.</p>
            <hr>

            <label for="name"><b>Name</b></label><br>
            <input type="text" placeholder="Enter Full Name" name="name" id="name" required><br><br>


            <label for="email"><b>E Mail</b></label><br>
            <input type="email" placeholder="Enter your Email Address" name="email" id="email" required><br><br>

            <label for="psd"><b>Password</b></label><br>
            <input type="password" placeholder="Create your Password" name="psd" id="psd required"><br><br>

            <label for="job"><b>Job</b></label><br>
            <select name="job" id="job" required>
                <option value=" ">Select Work</option>
                <option value="Maid">Maid</option>
                <option value="Electrician">Electrician</option>
                <option value="Mechanic">Mechanic</option>
                <option value="Plumber">Plumber</option>
            </select>
            <br><br>

            <label for="Phone"><b>Phone Number</b></label><br>
            <input type="tel" placeholder="Enter your Phone Number" name="Phone" id="Phone" required><br><br>

            <label for="Gender Select"><b>Please select your gender:</b></label><br>
            <select name="Gender" id="Gender" required>
                <option value=" ">Select Gender</option>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
                <option value="Trans">Transgender</option>
            </select>
            <br><br>

            <label for="PinCode"><b>PIN CODE</b></label><br>
            <input type="text" placeholder="Enter PIN CODE" name="PinCode" id="PIN CODE" required><br><br>

            <label for="AADHAR"><b>AADHAR Number</b></label><br>
            <input type="text" placeholder="Enter your AADHAR Number" name="AADHAR" id="AADHAR" required><br><br>

            <label for="JobType"><b>Job Type</b></label><br>
            <select name="SelectJobType" id="SelectJobType" required>
                <option value=" ">Select Type</option>
                <option value="Part Time">Part Time</option>
                <option value="Full Time">Full Time</option>
            </select><br><br>

            <label for="JobExperience"><b>Job Experience</b></label><br>
            <input type="text" placeholder="Enter your Experience in years" name="Experience" id="Experience" required><br><br>

            
            <label for="ExpectedServiceCost"><b>Expected Service Cost</b></label><br>
            <input type="text" placeholder="Enter your Expected Service Cost" name="ServiceCost" id="ServiceCost" required><br><br>

            <hr>
            <p>By creating an account you agree to our <a href="#">Terms & Privacy</a>.</p>

            <button type="submit" class="registerbtn">Register</button>
            <br>
            <div class="container signin">
                <p>Already have an account? <br>
                <a href="Login_Form.php">Log in</a>.</p>
            </div>
        </div>

        <footer>
            <div class="col-md-12">
                <div class="contactUs" id="contactUs">
                    <h3>Contact us</h3>
                    <h5>Em@il Id: <a href="https:\\www.gmail.com" target="_blank">weneedhelpinghands@gmail.com</a></h5>
                    <h5>TelePhone:<span itemprop="telephone"><a href="tel:+123456890">1800 202 9898</a></span></h5>
                    <h5>Address:We Need Helping hands Private Limited,<br>Buildings Alyssa, Begonia & Clove Embassy Tech Village,<br> Outer Ring Road, Devarabeesanahalli Village,<br> Bengaluru, 560103,<br> Karnataka, India</h5>
                </div>
            </div>

        </footer>
    </form>

</body>

</html>