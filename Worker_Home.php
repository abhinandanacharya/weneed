<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>WorkerHome</title>
    <link rel="Stylesheet" href="proCss.css">
    <link href="bootstrap.css" rel="stylesheet">
</head>

<body style="margin:0;background-color: white;">
    <form>
        <header>

            <nav>

                <input type="checkbox" id="check">
                <label for="check" class="checkbtn">
              <i class="fas fa-bars"></i>
            </label>
                <a href="Worker_Home.php"> <img class="logo" src="logoc.png"></a>
                <ul>
                    <li><a class="active" href="Worker_Home.php">Home</a></li>
                    <li><a href="#">About</a></li>
                   <li><a href="#">Contact</a></li>

                </ul>
                <!-- <ul>
                <li><label for="Worker_Login_Name" ><b>Login_Name</b></label></li>
                </ul> -->
            </nav>
        </header>

        <div class="workercontainer">
            <?php include 'Connection_PHP.php' ?>
            <?php
                $sql = "SELECT * FROM `Customer_login`";
                $result = mysqli_query($conn, $sql);
                while($row = mysqli_fetch_assoc($result)){
                    $Name = $row['Customer_Name'];
                    $Phone = $row['Customer_Phone'];
                    $Address = $row['Customer_Address'];
                    echo '<div class="notify_card">
                    <h3>Notification</h3><br>
                    <label for="name"><b>Name: </b></label><label for="Customer_Name"><b>'.$Name.'</b></label><br>
                    <label for="phone"><b>Phone: </b></label><label for="Customer_Phone"><b>'.$Phone.'</b></label><br>
                    <label for="address"><b>Address: </b></label><label for="Customer_address"><b>'.$Address.'</b></label><br>
                    <label for="date"><b>Booking-Date: </b></label><label for="Booking_Date"><b></b></label>
                </div>';
                }
            ?>
            
        </div>

    </form>
</body>

</html>