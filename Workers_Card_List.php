<html>

<head>
    <title>House Maid</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="proCss.css">
    <link href="bootstrap.css" rel="stylesheet" />


    <body style="background-color:sandybrown;">
        <form>
            <header>

                <nav>

                    <input type="checkbox" id="check">
                    <label for="check" class="checkbtn">
                      <i class="fas fa-bars"></i>
                    </label>
                    <a href="project1.php"> <img class="logo" src="logoc.png"></a>
                    <ul>
                        <li><a class="active" href="project1.php">Home</a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>

                    <ul style="float: right !important; color: white;">
                        <li><a href="Login_Form.php">Login</a>/<a href="Customer_SignUp_Form.php">Sign Up</a></li>

                    </ul>

                </nav>


            </header>
            <div class="workercontainer">

            <?php include 'Connection_PHP.php'; ?>
            
            <?php 
                $sql = "SELECT * FROM `worker_login`"; 
                $result = mysqli_query($conn, $sql);
                while($row = mysqli_fetch_assoc($result)){
                    $Name = $row['Worker_Name'];
                    $Phone = $row['Worker_Phone'];
                    $Gender = $row['Worker_Gender'];
                    $JobType = $row['Job_Type'];
                    $Expereince = $row['Experience'];
                    $ServiceCost = $row['Service_Cost'];

                    echo '<div class="job_card">
                    <div class="job_desc">
                        <div class="job_type">
                            <div class="job_left">
                                <img src="job.png">
                            </div>
                            <div class="job_right">
                                <div class="job">
                                    Job Type
                                </div>
                                <div class="time">
                                    '.$JobType.'
                                </div>
                            </div>
                        </div>

                        <div class="job_type">
                            <div class="job_left">
                                <img src="clock.png">
                            </div>
                            <div class="job_right">
                                <div class="job">
                                    Job Experience
                                </div>
                                <div class="time">
                                    '.$Expereince.' Years
                                </div>
                            </div>
                        </div>

                        <div class="job_type">
                            <div class="job_left">
                                <img src="rupee.png">
                            </div>
                            <div class="job_right">
                                <div class="job">
                                    Expected Salary
                                </div>
                                <div class="time">
                                    Rs. '.$ServiceCost.'
                                </div>
                            </div>

                        </div>
                    </div> <br>

                    <div class="details">
                        <label for="Name">Name: </label>
                        <label for="workerName">'.$Name.'</label><br>

                        <label for="Phone">Contact No.: </label>
                        <label for="Worker_Phone"><span itemprop="telephone"><a href="tel:'.$Phone.'"  Style="color:black;">'.$Phone.'</a></span></label><br>

                        <label for="Gender">Gender: </label>
                        <label for="Worker_Gender">'.$Gender.'</label><br>
                    </div>

                     <button type="submit" class="BookButton">BOOK</button>
                    
                </div>';
                }
            ?>
                
            </div>
            <footer>
                <!-- <div class="container-fluid"> -->
                <div class="col-md-12">
                    <div class="contactUs" id="contactUs">
                        <h3>Contact us</h3>
                        <h5>Em@il Id: <a href="https:\\www.gmail.com" target="_blank">weneedhelpinghands@gmail.com</a></h5>
                        <h5 style="line-height: 1.6;">TelePhone:<span itemprop="telephone"><a href="tel:+123456890">1800 202 9898</a></span></h5>
                        <h5 style="line-height: 1.6;">Address:We Need Helping hands Private Limited,<br>Buildings Alyssa, Begonia & Clove Embassy Tech Village,<br> Outer Ring Road, Devarabeesanahalli Village,<br> Bengaluru, 560103,<br> Karnataka, India</h5>
                    </div>
                </div>
                <!-- </div> -->
            </footer>
        </form>


    </body>

</html>