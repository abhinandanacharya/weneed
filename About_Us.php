<?php
session_start();
//include 'head.php';
if(isset($_SESSION['userid'])){
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="proCss.css">
    <title>CustomerSignUp</title>
</head>

<body>
    <header>

        <nav>

            <input type="checkbox" id="check">
            <label for="check" class="checkbtn">
                <i class="fas fa-bars"></i>
            </label>
            <a href="project1.php"> <img class="logo" src="logoc.png"></a>
            <ul>
                <li><a class="active" href="project1.php">Home</a></li>
                <li><a href="About_Us.php">About</a></li>
                <li><a href="#">Contact</a></li>

            </ul>

            <ul style="float: right !important; color: white;">
                <li><a href="Login_Form.php">Login</a>/<a href="CustomerSignUp.html">Sign Up</a></li>

            </ul>

        </nav>


    </header>
<div class="workercontainer">
    <div style="border:1px solid black; margin-top:5%; margin-left:15%; margin-bottom:5%; width:70%; padding:3%; text-align:center; background-color:white;">
        <h1 style="margin:0;">About WeNEED</h1>
        <br><br>
        <p>
            2020 and 2021 have been very tough years for everyone. No one was ready for the after effects of the Pandemic. Everyone was affected by the Pandemic somehow. 
            But the most affected people are the daily earners. Who lost everything their Savings, their Jobs, and some 
            families lost their love ones.
            <br> So, to Help them getting their Jobs back or bring new oppertunities to them. WeNEED gives them a platform 
            to come up and get a job/work of their respected feild.
            

        </p>
    </div>
</div>

    <footer>
            <div class="col-md-12">
                <div class="contactUs" id="contactUs">
                    <h3>Contact us</h3>
                    <h5>Em@il Id: <a href="https:\\www.gmail.com" target="_blank">weneedhelpinghands@gmail.com</a></h5>
                    <h5>TelePhone:<span itemprop="telephone"><a href="tel:+123456890">1800 202 9898</a></span></h5>
                    <h5>Address:We Need Helping hands Private Limited,<br>Buildings Alyssa, Begonia & Clove Embassy Tech Village,<br> Outer Ring Road, Devarabeesanahalli Village,<br> Bengaluru, 560103,<br> Karnataka, India</h5>
                </div>
            </div>

        </footer>
<?php
}
else{
    print_r($_SESSION);
    exit;
    ?>
    <script>
    alert("login first");
    window.location = "LoginTest.php";
</script>
    <?php
}
?>
