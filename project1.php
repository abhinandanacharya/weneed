<?php   session_start(); ?>
<html>

<head>
    <script src="float-panel.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>We Need</title>
    <link rel="stylesheet" href="proCss.css">
    <link rel="stylesheet" href="bootstrap.css">
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
</head>

<body>
    <form action="project1_PHP.php" method="post">
        <header>

            <nav>

                <input type="checkbox" id="check">
                <label for="check" class="checkbtn">
                  <i class="fas fa-bars"></i>
                </label>
                <a href="project1.php"> <img class="logo" src="logoc.png"></a>
                <ul>
                    <li><a class="active" href="project1.php">Home</a></li>
                    <li><a href="#">About</a></li>
                    <li><a href="#">Contact</a></li>

                </ul>

                <ul style="float: right !important; color: white;">
                    <?php if(isset($_SESSION['userid'])){
                        ?>
                             <li><a href="Logout.php">Logout</a></li>
                        <?php
                    }
                    else{
                        ?>
                        <li href="LoginTest.php"><a>Login</a>/<a href="Customer_SignUp_Form.php">Signup</a></li>
                        <?php
                    }
                   
                        ?>
                </ul>

            </nav>


        </header>



        <div class="col-md-12">
            <div class="migrant">
                <img src="migrant.jpg" class="migimg">
                <div class="explorer">
                    <h3 style="margin-left:5%;">WE NEED WORK!</h3>
                </div>
            </div>
        </div>
</form>
<form action="Maid_PHP.php" method="post">


        <div class="col-md-12 section" style="background-color: antiquewhite;">
            <div class="col-md-6">
                <img src="clean.jpg" class="limages">
            </div>

            <div class="col-md-6">
                <div class="rcontent">
                    <h1>House Maids</h1>
                    <h5 class="overflowtext textpad">Full time or Part Time Maids for your Home Cleaning, Cooking, Dish Washing etc.
                    </h5>
                    <button type="submit" name="Maid">Get a Maid</button>
                </div>
            </div>
        </div>
        </form>
<form action="Mechanic_PHP.php" method="post">

        <div class="col-md-12 section" style="background-color: antiquewhite;">
            <div class="col-md-6">
                <div class="lcontent">
                    <h1>Mechanics</h1>
                    <h5 class="overflowtext textpad">Mechanic for 4 Wheelers or 2 Wheeler Vehicles at your Doorstep.</h5>
                    <button type="submit" name="Mechanic">Get a Mechanic</button>
                </div>
            </div>

            <div class="col-md-6">
                <img src="mechanic.jpg" class="rimages">
            </div>
        </div>

</form>
<form action="Plumber_PHP.php" method="post">

        <div class="col-md-12 section" style="background-color: antiquewhite;">
            <div class="col-md-6">
                <img src="plumber.jpg" class="limages">
            </div>

            <div class="col-md-6">
                <div class="rcontent">
                    <h1>Plumber</h1>
                    <h5 class="overflowtext textpad">Water Leakage, Blockage, Repairing, new Faucet installation etc.</h5>
                    <button type="submit" name="Plumber">Get a Plumber</button>
                </div>
            </div>
        </div>
</form>
<form action="Electrician_PHP.php" method="post">

        <div class="col-md-12 section" style="background-color: antiquewhite;">
            <div class="col-md-6">
                <div class="lcontent">
                    <h1>Electrician</h1>
                    <h5 class="overflowtext textpad">Any thing related to electricals like Lights, fan, Switch Boards, M.C.B., etc.
                    </h5>
                    </p>
                    <button type="submit" name="Electrician">Get a Electrician</button>
                </div>
            </div>

            <div class="col-md-6">
                <img src="electrician.jpg" class="rimages">
            </div>
        </div>


        </form>

        <footer>
            <div class="col-md-12">
                <div class="contactUs" id="contactUs">
                    <h3>Contact us</h3>
                    <h5>Em@il Id: <a href="https:\\www.gmail.com" target="_blank">weneedhelpinghands@gmail.com</a></h5>
                    <h5>TelePhone:<span itemprop="telephone"><a href="tel:+123456890">1800 202 9898</a></span></h5>
                    <h5>Address:We Need Helping hands Private Limited,<br>Buildings Alyssa, Begonia & Clove Embassy Tech Village,<br> Outer Ring Road, Devarabeesanahalli Village,<br> Bengaluru, 560103,<br> Karnataka, India</h5>
                </div>
            </div>

        </footer>
    
</body>

</html>