<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="proCss.css">
    <link rel="stylesheet" href="bootstrap.css">
</head>

<body style="background-color:sandybrown;">
    
    <header>

<nav>

    <input type="checkbox" id="check">
    <label for="check" class="checkbtn">
      <i class="fas fa-bars"></i>
    </label>
    <a href="project1.php"> <img class="logo" src="logoc.png"></a>
    <ul>
        <li><a class="active" href="project1.php">Home</a></li>
        <li><a href="#">About</a></li>
        <li><a href="#">Contact</a></li>

    </ul>

    <ul style="float: right !important; color: white;">
        <li><a href="Login_Form.php">Login</a>/<a href="Customer_SignUp_Form.php">Sign Up</a></li>

    </ul>

</nav>


</header>
<form action="LoginPHP.php" method="post">

        <div class="Wform" style="width:25%;">
            <h1>Register</h1>
            <p>Please fill in this form to create an account.</p>
            <hr>

            <label for="email"><b>E Mail</b></label><br>
            <input type="email" placeholder="Enter your Email Address" name="email" id="email" required><br><br>

            <label for="psd"><b>Password</b></label><br>
            <input type="password" placeholder="Enter your Password" name="psd" id="psd required"><br><br>
            
            <label for="Cust_Worker"><b>I'm a</b></label>
            <select name="Select_Cust_Worker" id="Select_Cust_Worker" required>
                <option value="">Select</option>
                <option value="Customer">Customer</option>
                <option value="Worker">Worker</option>
            </select><br><br>

            <button type="submit" class="registerbtn">Login</button>
            <br>
            <hr style="width:100%; float:center;">
            <div class="signin">
                <p>Don't have an account?
                    <br><a href="Worker_SignUp_Form.php">Sign up</a>.
                </p>
            </div>

            <br>
        </div>
        
        <footer>
            <div class="col-md-12">
                <div class="contactUs" id="contactUs">
                    <h3>Contact us</h3>
                    <h5>Em@il Id: <a href="https:\\www.gmail.com" target="_blank">weneedhelpinghands@gmail.com</a></h5>
                    <h5>TelePhone:<span itemprop="telephone"><a href="tel:+123456890">1800 202 9898</a></span></h5>
                    <h5>Address:We Need Helping hands Private Limited,<br>Buildings Alyssa, Begonia & Clove Embassy Tech Village,<br> Outer Ring Road, Devarabeesanahalli Village,<br> Bengaluru, 560103,<br> Karnataka, India</h5>
                </div>
            </div>

        </footer>
    
</body>

</html>